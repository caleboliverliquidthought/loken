import LeaderboardRow from "@/components/LeaderboardRow";
import Card from "@/components/cards/Card";
import CardLayout from "@/components/cards/CardLayout";
import Link from "next/link";

function Page() {
  // define dummy users
  const users = [
    { rank: 1, firstName: "User", lastName: "Joe", points: 100 },
    { rank: 2, firstName: "User", lastName: "Joe", points: 90 },
    { rank: 3, firstName: "User", lastName: "Joe", points: 85 },
    { rank: 4, firstName: "User", lastName: "Joe", points: 45 },
    { rank: 5, firstName: "User", lastName: "Joe", points: 67 }
  ];

  return (
    <div className="">
      {/* row cards */}
      <div className="flex flex-row gap-2">
        <Card
          title="💰 YOUR BALANCE"
          buttonTitle="Redeem"
          points={"💧50"}
          route="/dashboard/redeem"
        />
        <Card
          title="🏆  LOKENS TO GIVE"
          buttonTitle="Award"
          points={"💧5"}
          route="/dashboard/award"
        />
      </div>

      {/* leaderboard */}
      <div className="mt-5">
        <CardLayout>
          <div>
            <h5 className="text-[10px] font-semibold">
              🏁 LEADERBOARD JUNE 2023
            </h5>
          </div>

          <div>
            {users.map((user) => (
              <LeaderboardRow key={user.rank} {...user} />
            ))}
            <div className="pt-2 flex justify-center items-center">
              <Link href="/dashboard/leaderboard">
                <button className="text-[10px] font-semibold text-[#3A7FEF]">
                  View Full Leaderboard
                </button>
              </Link>
            </div>
          </div>
        </CardLayout>
      </div>
    </div>
  );
}

export default Page;
