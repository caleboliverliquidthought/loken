"use client"
import "./inputfield.css"
import React, { useState } from 'react';

function InputField() {
  const [inputValue, setInputValue] = useState('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(event.target.value);
  };

  return (
    <div className="input-field">
      <input
        type="text"
        value={inputValue}
        onChange={handleChange}
        placeholder="Type here"
      />
      
    </div>
  );
}

export default InputField;