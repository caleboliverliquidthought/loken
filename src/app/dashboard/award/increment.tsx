"use client"
import "./increment.css";
import React, { useState } from 'react';

function Increment() {
  const [count, setCount] = useState(0);

  const incrementCount = () => {
    setCount(prevCount => {
      if (prevCount < 5) {
        return prevCount + 1;
      }
      return prevCount;
    });
  };

  const decrementCount = () => {
    setCount(prevCount => {
      if (prevCount > 0) {
        return prevCount - 1;
      }
      return prevCount;
    });
  };

  return (
    <div className='count-cont inline-block align-middle text-center'>
    <button className="w-10 h-10 inline-block align-middle border rounded-full" onClick={decrementCount}> - </button>
      <h1>💧{count}</h1>
      <button className="w-10 h-10 border rounded-full" onClick={incrementCount} w-96> + </button>
    </div>
  );
}

export default Increment;
