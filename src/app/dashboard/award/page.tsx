import Increment from "./increment";
import AwardCard from "@/components/cards/AwardCard";

function page() {
  return (
    <div className="flex justify-center">
      {/* row cards */}
      <div className="flex flex-row">
        <AwardCard
          title="Select Lokens to award"
          buttonTitle="Select Collegue"
        />
      </div>
    </div>
  );
}

export default page;
