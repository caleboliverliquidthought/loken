"use client";
import { useState } from "react";
import RewardCard from "@/components/cards/RewardCard";
import Modal from "@/components/Modal";

function Page() {
  const rewards = [
    { title: "Reward 1", points: "💧50" },
    { title: "Reward 2", points: "💧50" },
    { title: "Reward 3", points: "💧50" },
    { title: "Reward 4", points: "💧50" }
  ];

  const activeRewards = [{ title: "Reward 1", points: "💧50" }];

  const [selectedReward, setSelectedReward] = useState<any>(null);

  const openModal = (reward: any) => {
    setSelectedReward(reward);
  };

  const closeModal = () => {
    setSelectedReward(null);
  };

  return (
    <div>
      <div className="text-white">Back</div>
      <div className="flex flex-col text-center items-center justify-center text-lg text-white space-y-5 mb-5">
        <span className="text-lg text-white">Your Balance</span>
        <span className="text-[45px]">💧50</span>
      </div>

      <div className="grid grid-cols-2 gap-3 mb-10 mx-auto">
        {rewards.map((reward, index) => (
          <div key={index} onClick={() => openModal(reward)}>
            <RewardCard title={reward.title} points={reward.points} />
          </div>
        ))}
      </div>

      {/* active reward */}
      <div>
        <span>Your rewards</span>
        <div className="p-5 border bg-gray-100 border-gray-500 rounded-xl flex-grow">
          {activeRewards.map((reward, index) => (
            <div key={index}>{reward.title}</div>
          ))}
        </div>
      </div>

      {selectedReward && (
        <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50">
          <Modal
            points={selectedReward.points}
            reward={selectedReward.title}
            onClose={closeModal}
          />
        </div>
      )}
    </div>
  );
}

export default Page;
