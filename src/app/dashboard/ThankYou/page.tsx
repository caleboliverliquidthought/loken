
import ThankYouCard from "@/components/cards/ThankYouCard";

function page() {
    return (
        <div className="flex justify-center">
        {/* row cards */}
        <div className="flex flex-row">
          <ThankYouCard title="🙌 Thank you! They’ve been notified."
          buttonTitle="Dashboard" />
           
        </div>
      </div>
    );
  }
  
  export default page;
  