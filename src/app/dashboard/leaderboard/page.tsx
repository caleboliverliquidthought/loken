import LeaderboardRow from "@/components/LeaderboardRow";
import CardLayout from "@/components/cards/CardLayout";

const users = [
  { rank: 1, firstName: "User", lastName: "Joe", points: 100 },
  { rank: 2, firstName: "User", lastName: "Joe", points: 90 },
  { rank: 3, firstName: "User", lastName: "Joe", points: 85 },
  { rank: 4, firstName: "User", lastName: "Joe", points: 45 },
  { rank: 5, firstName: "User", lastName: "Joe", points: 67 },
  { rank: 6, firstName: "User", lastName: "Joe", points: 67 },
  { rank: 7, firstName: "User", lastName: "Joe", points: 67 },
  { rank: 8, firstName: "User", lastName: "Joe", points: 67 },
  { rank: 9, firstName: "User", lastName: "Joe", points: 67 },
  { rank: 10, firstName: "User", lastName: "Joe", points: 67 },
  { rank: 11, firstName: "User", lastName: "Joe", points: 67 }
];

function leaderboard() {
  return (
    <div className="mt-5">
      <CardLayout>
        <div>
          <h5 className="text-[10px] font-semibold">
            🏁 LEADERBOARD JUNE 2023
          </h5>
        </div>

        <div>
          {users.map((user) => (
            <LeaderboardRow key={user.rank} {...user} />
          ))}
        </div>
      </CardLayout>
    </div>
  );
}

export default leaderboard;
