"use client"
import React, { useState } from 'react';
import "./reason-options.css";
import InputField from '../collegues/inputfield';

function RadioButtons() {
  const [selectedOption, setSelectedOption] = useState('');

  const handleOptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedOption(event.target.value);
  };

  return (
    <div className='radio-btns'>
      <label>
        <input
          type="radio"
          value="option1"
          checked={selectedOption === 'option1'}
          onChange={handleOptionChange}
        />
        Driving Brand Values
      </label>
      <br/>
      <label>
        <input
          type="radio"
          value="option2"
          checked={selectedOption === 'option2'}
          onChange={handleOptionChange}
        />
        Excellence in craft
      </label>
      <br/>
      <label>
        <input
          type="radio"
          value="option3"
          checked={selectedOption === 'option3'}
          onChange={handleOptionChange}
        />
        Blowing client’s socks off
      </label>
      <br/>
      <label>
        <input
          type="radio"
          value="option4"
          checked={selectedOption === 'option4'}
          onChange={handleOptionChange}
        />
        Future focused
      </label>
      <br/>
      <label>
        <input
          type="radio"
          value="option5"
          checked={selectedOption === 'option5'}
          onChange={handleOptionChange}
        />
        Digital Transformation
      </label>
      <br/><br/>
      <label className='label-reason'>Have a Special Reason?</label>
      <InputField />
    </div>
  );
}

export default RadioButtons;