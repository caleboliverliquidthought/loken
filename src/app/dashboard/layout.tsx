import Header from "@/components/Header";

export default function DashboardLayout({
  children
}: {
  children: React.ReactNode;
}) {
  return (
    <section className="relative">
      <Header />
      <div className="absolute top-[80px] w-full px-5">{children}</div>
    </section>
  );
}
