import LeaderboardRow from "@/components/LeaderboardRow";
import AdminCategoriesRow from "@/components/AdminCategoriesRow";
import AdminRewardsRow from "@/components/AdminRewardsRow";
import Card from "@/components/cards/Card";
import CardLayout from "@/components/cards/CardLayout";
import Link from "next/link";

function Page() {
  // define dummy users
  const users = [
    { rank: 1, firstName: "User", lastName: "Joe", points: 100 },
    { rank: 2, firstName: "User", lastName: "Joe", points: 90 },
    { rank: 3, firstName: "User", lastName: "Joe", points: 85 },
    { rank: 4, firstName: "User", lastName: "Joe", points: 45 },
    { rank: 5, firstName: "User", lastName: "Joe", points: 67 }
  ];
    
  const rewards = [
    { rank: 1, rewardName: "Free Lunch" },
    { rank: 2, rewardName: "Vidae Voucher" },
    { rank: 3, rewardName: "Half-Day Leave" },
    { rank: 4, rewardName: "Full-Day Leave" }
  ];    
    
  const categories = [
    { rank: 1, categoryName: "Driving Brand Values" },
    { rank: 2, categoryName: "Excellence in craft" },
    { rank: 3, categoryName: "Blowing client's socks off" },
    { rank: 4, categoryName: "Future Focused" }
  ];      

  return (
    <div className="flex flex-row gap-5 w-full max-w-screen-xl mx-auto">
      <div className="w-6/12">
      {/* row cards */}
      <div className="flex flex-row gap-5 ">
        <Card
          title="💰 YOUR BALANCE"
          buttonTitle="Redeem"
          points={"💧50"}
          route="/dashboard/redeem"
         className="w-6/12"    
        />
        <Card
          title="🏆  LOKENS TO GIVE"
          buttonTitle="Award"
          points={"💧5"}
          route="/dashboard/award"
         className="w-6/12"    
        />
      </div>
      {/* Rewards */}
       
    <div className="mt-5">
            <CardLayout>
          <div>
            <h5 className="text-[10px] font-semibold">
              REWARDS
            </h5>
          </div>

          <div>
            {rewards.map((rewards) => (
              <AdminRewardsRow key={rewards.rank} {...rewards} />
            ))}
          </div>
        </CardLayout>      
    </div> 
      {/* Categories */}
    <div className="mt-5">
            <CardLayout>
          <div>
            <h5 className="text-[10px] font-semibold">
              CATEGORIES
            </h5>
          </div>

          <div>
            {categories.map((categories) => (
              <AdminCategoriesRow key={categories.rank} {...categories} />
            ))}
          </div>
        </CardLayout>      
    </div>      
          
      
      
      </div>
      
      {/* leaderboard */}
      <div  className="w-6/12">
        <CardLayout>
          <div>
            <h5 className="text-[10px] font-semibold">
              MEMBERS
            </h5>
          </div>

          <div>
            {users.map((user) => (
              <LeaderboardRow key={user.rank} {...user} />
            ))}
            <div className="pt-2 flex justify-center items-center">
              <Link href="/dashboard/leaderboard">
                <button className="text-[10px] font-semibold text-[#3A7FEF]">
                  View Full Leaderboard
                </button>
              </Link>
            </div>
          </div>
        </CardLayout>
      </div>
    </div>
  );
}

export default Page;
