'use client';
import { useState } from 'react';
import "./page.css";
import Link from "next/link";

function page() {
const [count, setCount] = useState(0);
return <>

<div className="home-background">
 <nav>
  <div><img src="assets/images/LOKENS.png" alt="LOKENS"/></div>
  <div className="navlinks"><Link href="/dashboard">Register</Link><Link href="/login">Login</Link></div>
 </nav>
 <div className="video-holder">
  <div className="video-container" onClick={() => setCount(count + 1)}>
  {count==0?(<img src="assets/images/vid.jpg" alt="Video"/>):(<div><iframe width="871" height="490" src="https://www.youtube.com/embed/slnrHJdrvXY?autoplay=1&rel=0&modestbranding=1&controls=0" title="About Lokens" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe></div>)}
 </div>
</div>
</div>
  </>;
}

export default page;
