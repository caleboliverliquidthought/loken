import React from 'react';

const Login: React.FC = () => {
  return (
    <div className="flex items-center justify-center h-screen bg-blue-500">
      <div className="bg-white shadow-md rounded px-8 py-10 max-w-sm w-full space-y-4">
        <h2 className="text-2xl font-semibold text-center">Login</h2>
        <form className="space-y-4">
          <div>
            <label htmlFor="email" className="block font-medium">
              Email
            </label>
            <input
              id="email"
              type="email"
              className="w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring focus:ring-indigo-500 focus:ring-opacity-50"
            />
          </div>
          <div>
            <label htmlFor="password" className="block font-medium">
              Password
            </label>
            <input
              id="password"
              type="password"
              className="w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring focus:ring-indigo-500 focus:ring-opacity-50"
            />
          </div>
          <div>
            <button
              type="submit"
              className="w-full bg-indigo-500 text-white font-semibold rounded-md py-2"
            >
              Log in
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Login;
