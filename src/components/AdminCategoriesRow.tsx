
interface AdminCategoriesRowProps {
  rank: number;
  categoryName: string;
}

function AdminCategoriesRow({
  rank,
  categoryName
}: AdminCategoriesRowProps) {
  return (
    <div className="flex justify-between items-center border-b border-gray-200 py-2">
      <div className="flex items-center gap-2">
        <div>{rank}</div>
        <div>{categoryName}</div>
      </div>
      <div className="flex items-center gap-2">
        <div>Edit</div>
      </div>
    </div>
  );
}

export default AdminCategoriesRow;
