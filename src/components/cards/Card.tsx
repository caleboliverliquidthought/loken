import Link from "next/link";

interface CardProps {
  title: string;
  titleIcon?: React.ReactNode;
  icon?: React.ReactNode;
  points?: string;
  buttonTitle: string;
  route: string;
}

function Card({
  title,
  icon,
  titleIcon,
  points,
  buttonTitle,
  route
}: CardProps) {
  return (
    <div className="w-[180px] p-5 h-[190px] flex flex-col justify-center bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <h5 className="text-[10px] font-semibold text-center">{title}</h5>

      <div className="flex flex-row py-7  justify-center">
        <div className=" text-4xl font-bold">{points}</div>
      </div>
      <a
        href={route}
        className="inline-flex items-center px-3 py-2 text-sm font-medium text-white bg-[#3A7FEF] rounded-sm hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800 w-full text-center"
      >
        {buttonTitle}
      </a>
    </div>
  );
}

export default Card;
