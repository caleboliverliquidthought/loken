import Link from "next/link";

interface CardProps {
  children: React.ReactNode;
}

function CardLayout({ children }: CardProps) {
  return (
    <div className="p-5 flex flex-grow flex-col bg-white border border-gray-200 rounded-lg shadow-2xl dark:bg-gray-800 dark:border-gray-700 h-full">
      {children}
    </div>
  );
}

export default CardLayout;
