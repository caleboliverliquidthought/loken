interface CardProps {
  title: string;
  points?: string;
}

function RewardCard({ title, points }: CardProps) {
  return (
    <div className="w-[150px] p-5 h-[130px] text-white border border-gray-200 rounded-lg shadow">
      <h5 className="text-[16px] font-semibold mb-12">{title}</h5>

      <div className=" flex justify-end text-md">{points}</div>
    </div>
  );
}

export default RewardCard;
