import Increment from "@/app/dashboard/award/increment";
import SearchBar from "@/app/dashboard/collegues/SearchBar";
import RadioButtons from "@/app/dashboard/reason/reason-options";
import Link from "next/link";


interface CardProps {
  title: string;
  titleIcon?: React.ReactNode;
  icon?: React.ReactNode;
  points?: number;
  buttonTitle: string;
}

function ReasonCard({ title, icon, titleIcon, points, buttonTitle }: CardProps) {
  return (
    <div>
    <div className="w-full h-[500px] pt-6 pr-6 pl-6 bg-white border border-gray-200 rounded-lg shadow">
      <h5 className="text-[26px] text-center font-600 font-semibold">{title}</h5>

      <div className="flex justify-center flex-row">
        {icon && <div className="mr-2">{icon}</div>}
        <div className="text-2xl font-bold">{points}</div>
      {/* <SearchBar/> */}
      </div>

      {/* <p className="mb-3 font-normal text-gray-700 dark:text-gray-400">
        Here are the biggest enterprise technology acquisitions of 2021 so far,
        in reverse chronological order.
      </p> */}

      <div className="flex justify-center">

        <RadioButtons/>
      </div>
      <div className="flex justify-center">
        <a
            href="/dashboard/ConfirmAward"
            className="inline-flex w-[214px] items-center justify-center px-6 py-4 text-sm font-medium text-center text-white bg-orange-400 rounded-lgfocus:ring-4"
        >
        
        {buttonTitle}
        <svg
          aria-hidden="true"
          className="w-4 h-4 ml-2 -mr-1"
          fill="currentColor"
          viewBox="0 0 20 20"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fill-rule="evenodd"
            d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
            clip-rule="evenodd"
          ></path>
        </svg>
      </a>
      </div>
      </div>
      <div className="flex p-4 justify-center underline underline-offset-1">
        <a href="/dashboard/collegues"> Cancel </a>
        </div>
    </div>
    
    
  );
}

export default ReasonCard;
