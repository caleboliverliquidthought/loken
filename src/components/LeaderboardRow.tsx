import Avatar from "./Avatar";

interface LeaderboardRowProps {
  rank: number;
  firstName: string;
  profilePicture?: string;
  lastName: string;
  points: number;
}

function LeaderboardRow({
  rank,
  firstName,
  lastName,
  profilePicture,
  points
}: LeaderboardRowProps) {
  return (
    <div className="flex justify-between items-center border-b border-gray-200 py-2">
      <div className="flex items-center gap-2">
        <div>{rank}</div>
        <Avatar size={20} />
        <div>{firstName}</div>
        <div>{lastName.charAt(0)}.</div>
      </div>
      <div className="flex items-center gap-2">
        <div>💧 {points}</div>
      </div>
    </div>
  );
}

export default LeaderboardRow;
