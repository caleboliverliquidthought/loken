interface AvatarProps {
  src?: string;
  alt?: string;
  size: number;
}

function Avatar({ src, alt, size = 48 }: AvatarProps) {
  return (
    <div className="flex flex-row items-center gap-2">
      <div
        style={{ height: `${size}px`, width: `${size}px` }}
        className="rounded-full bg-gray-400"
      ></div>
    </div>
  );
}

export default Avatar;
