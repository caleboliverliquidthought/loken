import { DownArrow } from "@/assets/icons/DownArrow";
import Avatar from "./Avatar";

function Header() {
  return (
    <div className=" h-[240px] bg-[#59AFFF] ">
      <div className="flex flex-row justify-between items-center px-5 py-4">
        <div className=" text-white text-2xl font-extrabold">LOKENS</div>
        <div className="flex flex-row items-center gap-2">
          <Avatar size={45} />
        </div>
      </div>
    </div>
  );
}

export default Header;
